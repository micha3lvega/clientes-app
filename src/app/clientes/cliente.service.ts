import { Injectable } from '@angular/core';
import { formatDate, DatePipe, registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';

import { Cliente } from './cliente';
import { CLIENTES } from './clientes.json';
import { of, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import swal from 'sweetalert2';
import { Router } from '@angular/router';



@Injectable()
export class ClienteService {

  private urlEndpoint = 'http://localhost:8081/api/clientes';
  private httpHeaders = new HttpHeaders({ 'content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) {
  }

  getClientes(): Observable<Cliente[]> {
    return this.http.get(this.urlEndpoint).pipe(
      tap(response => {
        const clientes = response as Cliente[];
        clientes.forEach(cliente => {
          console.log(cliente.nombre);
        });
      }),
      map(response => {
        const clientes = response as Cliente[];
        return clientes.map(cliente => {
          cliente.nombre = cliente.nombre.toUpperCase();
          cliente.createAt = formatDate(cliente.createAt, 'dd-MM-yyyy', 'en-US');
          // const datePipe = new DatePipe('en-US'); // Locale por defecto ingles

          // Configuraciones para lenguaje en español
          // registerLocaleData(localeES, 'es'); //Configurado globalmente en app.module
          const datePipe = new DatePipe('es');

          // cliente.createAt = datePipe.transform(cliente.createAt, 'dd-MM-yyyy'); // 31-12-2019
          // [No soportada en español]
          // cliente.createAt = datePipe.transform(cliente.createAt, 'EEE dd-MMM-yyyy'); // Dia de la semana y mes abreviados
          // cliente.createAt = datePipe.transform(cliente.createAt, 'EEEE dd, MMMM yyyy'); // Dia de la semana y mes completos
          return cliente;
        });
      }),
      tap(response => {
        const clientes = response as Cliente[];
        clientes.forEach(cliente => {
          console.log(cliente.nombre);
        });
      })
    );
  }

  create(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.urlEndpoint, cliente, { headers: this.httpHeaders }).pipe(
      catchError(e => {
        console.error(e);

        // Controlar errores del backend
        if (e.status == 400) {
          return throwError(e);
        }// fin if

        swal.fire('Errror al guardar el usuario', e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  findById(id): Observable<Cliente> {
    return this.http.get<Cliente>(this.urlEndpoint + '/' + id).pipe(
      catchError(e => {
        this.router.navigate(['/clientes']);
        console.error(e);
        swal.fire('Errror al obtener el usuario', e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Cliente> {
    return this.http.delete<Cliente>(this.urlEndpoint + '/' + id, { headers: this.httpHeaders }).pipe(
      catchError(e => {
        console.error(e);
        swal.fire('Errror al eliminar el usuario', e.error.error, 'error');
        return throwError(e);
      }));
  }

}
