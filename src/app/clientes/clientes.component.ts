import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { tap } from 'rxjs/operators';

import swal from 'sweetalert2';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  private clienteServices: ClienteService;

  constructor(clienteServices: ClienteService) {
    this.clienteServices = clienteServices;
  }

  ngOnInit() {
    this.clienteServices.getClientes().pipe(
      tap(clientes => {
        clientes.forEach(cliente => {
          console.log(cliente.nombre);
        });
      })
    ).subscribe(
      //      function(clientes){
      //         this.clientes = clientes;
      //      }
      clientes => this.clientes = clientes
    );
  }// Fin ngOnInit

  delete(cliente: Cliente): void {

    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    }); // Fin constantes swalWithBootstrapButtons

    swalWithBootstrapButtons.fire({
      title: '¿Esta seguro?',
      text: '¿Seguro que desea eliminar al cliente: ' + cliente.nombre + '?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Eliminalo!',
      cancelButtonText: 'No, Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.clienteServices.delete(cliente.id).subscribe(
          response => {
            this.clientes = this.clientes.filter(cli => cli !== cliente);
            swalWithBootstrapButtons.fire(
              'Eliminado!',
              'El cliente ' + cliente.nombre + ' ha sido eliminado.',
              'success'
            );
          }); // fin delete
      }// Fin if
    }); // fin swalWithBootstrapButtons.fire

  }// Fin metodo delete

}
