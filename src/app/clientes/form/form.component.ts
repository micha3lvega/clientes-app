import { Component, OnInit } from '@angular/core';
import { Cliente } from './../cliente';
import { ClienteService } from './../cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public cliente : Cliente = new Cliente();
  public title : string = "Nuevo cliente";
  public errors: string[];

  constructor(private clienteService : ClienteService, private router : Router , private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.findById();
  }

  public findById() : void{
    this.activatedRoute.params.subscribe(params => {
      let id  = params['id'];
      if(id){
        this.clienteService.findById(id).subscribe((cliente) => this.cliente = cliente)
      }
    });
  }

  public save() : void{
    this.clienteService.create(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes']);
        swal.fire('Exito','Cliente '+  this.cliente.nombre + ' creado con exito','success');
      },//Manejo de error en el metodo
      err => {
        console.error("Codigo de error backend: " + err.status);
        console.error(err.error.errors);
        this.errors = err.error.errors as string[];//Guardar los errores que vienen del backend
      }
    )
  }
}
