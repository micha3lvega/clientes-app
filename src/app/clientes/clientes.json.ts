import { Cliente} from './cliente';

export const CLIENTES : Cliente[] = [
  {id: 1, nombre: 'Andres', apellido: 'Guzman', createAt: '12-02-2018', email: 'me@example.com'},
  {id: 2, nombre: 'Andres', apellido: 'Guzman', createAt: '12-02-2018', email: 'me@example.com'},
  {id: 3, nombre: 'Andres', apellido: 'Guzman', createAt: '12-02-2018', email: 'me@example.com'},
];
